describe(' Preencher um campo na tela ', () => {
    it('Preenchendo o campo Modelo', () => {
        cy.visit('/')
        cy.get('#texto').type('Honda Civic LX')
    })
})

describe('Preenchendo um campo numérico', () => {
    it('Preenchendo o campo ano de fabricaçao', () => {
        cy.visit('/')
        cy.get('#numero').type('2000')
    })
})

describe('Selecionando um valor em um select', () => {
    it('Selecionando um valor', () => {
        cy.visit('/')
        cy.get('#select').select('Fit').should('have.value', 'Fit')
    })
})

describe('Preenchendo um campo de email', function () {
    it('Preenchendo o campo', () => {
        cy.visit('/')
        cy.get('#email').type('testes@teste.com.br')
    })
})

describe('Selecionando um checkbox', () => {
    it('Preenchendo selecionando checkbox', () => {
        cy.visit('/')
        //Seleciona todos os check box
        cy.get('[type="checkbox"]').check()
        //Seleciona um checkbox com um determinado id
        cy.get('#ar').check()
    })
})

describe('Selecionando um radiobox', () => {
    it('Selecionando o radiobox', () => {
        cy.visit('/')
        cy.get('#manual').check()
    })
})

describe('Preenchendo um textarea', () => {
    it('Preenchendo um valor', () => {
        cy.visit('/')
        cy.get('#descricao').type('Inserindo texto')
    })
})

describe('Clicando em um botão', () => {
    it('Clicando no botão', () => {
        cy.visit('/')
        cy.get('#enviar').click()
    })
})

describe('Preenchendo todos os campos do form', () => {
    it('Preenchendo todos os campos', () => {
        cy.visit('/')
        cy.get('#texto').type('Honda Civic LX')
        cy.get('#numero').type('2000')
        cy.get('#select').select('Fit').should('have.value', 'Fit')
        cy.get('#email').type('testes@teste.com.br')
        cy.get('[type="checkbox"]').check()
        cy.get('#manual').check()
        cy.get('#descricao').type('Inserindo texto')
        cy.get('#enviar').click()
    })
})

describe('Limpando um campo', () => {
    it('Limpando um campo', () => {
        cy.visit('/')
        cy.get('#descricao').type('Inserindo texto')
        cy.get('#descricao').clear()
    })
})